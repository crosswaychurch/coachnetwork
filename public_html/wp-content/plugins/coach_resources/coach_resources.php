<?php
/*
    Plugin Name: COACH Resources
    Plugin URI: http://www.jordesign.com
    Description: Setting up Resources functionality for COACH
    Author: Jordan Gillman
    Version: 1.0
    Author URI: http://www.jordesign.com
*/

//Add Resource CPT
// Register Custom Post Type
function add_coach_resources() {

    $labels = array(
        'name'                  => _x( 'Resources', 'Post Type General Name', 'coach' ),
        'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'coach' ),
        'menu_name'             => __( 'Resources', 'coach' ),
        'name_admin_bar'        => __( 'Resources', 'coach' ),
        'archives'              => __( 'Resource Archives', 'coach' ),
        'parent_item_colon'     => __( 'ParentResource:', 'coach' ),
        'all_items'             => __( 'All Resources', 'coach' ),
        'add_new_item'          => __( 'Add New Resource', 'coach' ),
        'add_new'               => __( 'Add New', 'coach' ),
        'new_item'              => __( 'New Resource', 'coach' ),
        'edit_item'             => __( 'Edit Resource', 'coach' ),
        'update_item'           => __( 'Update Resource', 'coach' ),
        'view_item'             => __( 'View Resources', 'coach' ),
        'search_items'          => __( 'Search Resources', 'coach' ),
        'not_found'             => __( 'Not found', 'coach' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'coach' ),
        'featured_image'        => __( 'Featured Image', 'coach' ),
        'set_featured_image'    => __( 'Set featured image', 'coach' ),
        'remove_featured_image' => __( 'Remove featured image', 'coach' ),
        'use_featured_image'    => __( 'Use as featured image', 'coach' ),
        'insert_into_item'      => __( 'Insert into item', 'coach' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'coach' ),
        'items_list'            => __( 'Items list', 'coach' ),
        'items_list_navigation' => __( 'Items list navigation', 'coach' ),
        'filter_items_list'     => __( 'Filter items list', 'coach' ),
    );
    $args = array(
        'label'                 => __( 'Resource', 'coach' ),
        'description'           => __( 'COACH Resources', 'coach' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', ),
        'taxonomies'            => array( 'resource_type' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-media-document',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'resources', $args );

}
add_action( 'init', 'add_coach_resources', 0 );

//Resource Category Taxonomy
// Register Custom Taxonomy
function add_resource_type() {

    $labels = array(
        'name'                       => _x( 'Resource Types', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Resource Type', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Resource Types', 'text_domain' ),
        'all_items'                  => __( 'All Resource Types', 'text_domain' ),
        'parent_item'                => __( 'Parent Resource Type', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Resource Type:', 'text_domain' ),
        'new_item_name'              => __( 'New Resource Type', 'text_domain' ),
        'add_new_item'               => __( 'Add Resource Type', 'text_domain' ),
        'edit_item'                  => __( 'Edit Resource Type', 'text_domain' ),
        'update_item'                => __( 'Update Resource Type', 'text_domain' ),
        'view_item'                  => __( 'View Resource Type', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Items', 'text_domain' ),
        'search_items'               => __( 'Search Items', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No items', 'text_domain' ),
        'items_list'                 => __( 'Items list', 'text_domain' ),
        'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'resource_type', array( 'resources' ), $args );

}
add_action( 'init', 'add_resource_type', 0 );

//Sort Resources Alphabetically
add_action( 'pre_get_posts', 'coach_sort_staff' );
function coach_sort_staff( $query ) {
    if ( $query->is_main_query() && !is_admin() ) {
        if ( $query->is_post_type_archive('resources') ) {
            $query->set('orderby', 'title');  
            $query->set('order', 'ASC'); 
            $query->set('posts_per_page', -1);

        }       
    }
} 

