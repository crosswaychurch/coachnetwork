<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'coachnet_wp' );

/** MySQL database username */
define( 'DB_USER', 'coachnet_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'astr080y' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Cv{#S(^:|{lLGs?:7{Zvty=4)>*.|[GN.@iFWfi;jVYzZxa{V!t0SZn|WTljaE(s');
define('SECURE_AUTH_KEY',  '~NH(U(2wys5ZQVXO#NCEj9i,+XS,|x!K$@Pf=Cd>p(|GN<g~;`s*0[.4vAkl%l2>');
define('LOGGED_IN_KEY',    '},,tp!8d,8=iH||hs`^_cE+p*ViRr#tSv&N/F}[p>:vAvA?VlA0|/;o*idPUHfh2');
define('NONCE_KEY',        'Wd2n-7B1M)&,o!RNmtODI}0Tj !C3|fP]*vN}XJ?{]r<kzR@zJ:Lhys^l9TQ:&uN');
define('AUTH_SALT',        'pl]llKFwD;8f0,mT{^<Z@%aCK$T0klZ:i8P_Tg3k$nghg1k6q^:~SL8-Driq33aL');
define('SECURE_AUTH_SALT', 'B}/d0:P@5M?6wfJG@dFwYQJD>Zi]=o#n%-%K^gZZka;sG[Pv Q}$E(_k9]??.YS3');
define('LOGGED_IN_SALT',   'j[5_2={c:RMfI4tX%2&QzzTLh5>M<ODEj*cV_ak5p*8u-qZcTLBh$;`=-?RxkUAK');
define('NONCE_SALT',       '7}c?s0p=rL4O1=F?WRu%3)TViwNQ=AclIn)72)y_zC:=amoZK{#Utg0fgS?02mLn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpcoach_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
